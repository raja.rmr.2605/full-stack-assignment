package com.surabi.billing_system.advice;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;

@Component
@Aspect
public class LoggingAdvice {

	//Logger
	Logger log = LoggerFactory.getLogger(LoggingAdvice.class);
	
	@Pointcut(value = "execution(* com.surabi.billing_system.*.*.*(..) )")
	public void myPointcut() {
		
	}
	
	//configuration
	@Around("myPointcut()")
	public Object applicationLogger(ProceedingJoinPoint pjp) throws Throwable {
		ObjectMapper mapper = new ObjectMapper();
		String methodName = pjp.getSignature().getName();
		String className = pjp.getTarget().getClass().toString();
		
		// this captures before advice
		Object[] array = pjp.getArgs();
		log.info("method invoked " + className + " : " + methodName + "() arguments : " + mapper.writeValueAsString(array));

		// this captures after response
		Object obj = pjp.proceed();
		log.info( className + " : " + methodName + "() Response: " + mapper.writeValueAsString(obj));
		return obj;
		
	}
}

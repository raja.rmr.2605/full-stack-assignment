package com.surabi.billing_system.controllers;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.surabi.billing_system.dao.AdminDAO;
import com.surabi.billing_system.dao.AdminDAOImpl;
import com.surabi.billing_system.dao.UserDAOImpl;
import com.surabi.billing_system.entities.User;
import com.surabi.billing_system.services.AdminService;
import com.surabi.billing_system.services.UserService;

@RestController
@RequestMapping("/admin")
@CrossOrigin(origins = "http://localhost:4200")
public class AdminController {
	
	// this variable helps us to know whether admin is logged in or not
	// intially admin will be not logged in
	private static  boolean loggedIn = false ;

	@Autowired
	UserService userService ;
	
	@Autowired
	AdminService adminService ;
	
	
	@PostMapping("/logIn")
	public HashMap logIn(@RequestBody HashMap<String, String> map) {
		
		//retrieve data from request body
		String name = map.get("name");
		String password = map.get("password");
		
		//check whether admin is valid or not
		loggedIn = adminService.isValid(name, password);
		HashMap<String,Boolean> res = new HashMap<String, Boolean>();
		if(loggedIn) {
			res.put("loggedin", true);
			return res;
		}else {
			res.put("loggedin", false);
			return res ;
		}
	}
	
	@GetMapping("/logOut")
	public void logOut() {
		loggedIn = false;
	}
	
	
	@GetMapping("/monthBill")
	public HashMap  getMonthBill() {
		HashMap<String,String> map = new HashMap();
		if(loggedIn) {
			map.put("res", "This month's total bill is " + adminService.getMonthBill());
			return map;
		}
		else {
			map.put("res", "login required");
			return map;
		}
	}
	
	@GetMapping("/dayBill")
	public HashMap getDayBill() {
		HashMap<String,String> map = new HashMap();
		if(loggedIn) {
			map.put("res", "Today's total bill is " + adminService.getDayBill());
			return map;
		}
		else {
			map.put("res", "login required");
			return map;
		}
		
	}
	
	//( create ) adding a user 
	@PostMapping("/users")
	public User addUser(@RequestBody User theUser) {
		
		if(loggedIn) {
			theUser.setId(0);
			userService.save(theUser);
			return theUser;
		}
		else {
			return new User();
		}
		
	}
	
	//(read) finding user by id  
	@GetMapping("/users/{id}")
	public User getUser(@PathVariable int id) {
			User user = userService.findById(id);
			return user;
	}
		
	//deleting user by id
	@DeleteMapping("/users/{id}")
	public User deleteUserById(@PathVariable int id) {
		
			User user = userService.findById(id);
			
			if(user == null) {
				return null;
			}
			userService.deleteById(id);
			return user;

	}
	
	//update
	@PutMapping("/users")
	public void updateUser(@RequestBody User theUser) {	
		System.out.println(theUser);
			userService.update(theUser); 	
	}
}

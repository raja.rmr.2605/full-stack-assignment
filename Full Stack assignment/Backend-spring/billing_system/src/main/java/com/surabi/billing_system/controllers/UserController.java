package com.surabi.billing_system.controllers;

import java.util.HashMap;
import java.util.List ;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.surabi.billing_system.entities.Bill;
import com.surabi.billing_system.entities.Menu;
import com.surabi.billing_system.entities.User;
import com.surabi.billing_system.services.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

	// this variable helps us to know whether user is logged in or not
	// intially admin will be not logged in
	private static boolean loggedIn = false ;
	
	@Autowired
	UserService userService;
	
	@PostMapping("/logIn")
	public HashMap logIn(@RequestBody HashMap<String, String> map) {
		
		//retrieve data from request body
		String name = map.get("name");
		String password = map.get("password");
		
		//check whether user is valid or not
		loggedIn = userService.isValid(name, password);
		HashMap<String, Boolean> res = new HashMap();
		res.put("valid", false);
		if(loggedIn) {
			res.put("valid", true);
		}
		return res;
	}
	
	// registering user
	@PostMapping("/register")
	public User register(@RequestBody User user) {
		userService.save(user);
		return user;
	}
	
	// to logout the user
	@GetMapping("/logOut")
	public String logOut() {
		loggedIn = false;
		return "LoggedOut successfully";
	}
	
	// displays Menu
	@GetMapping("/menu")
	public List<Menu> showMenu(){
		return userService.showMenu();
	}
	
	// For Taking Order from input
	@PostMapping("/order")
	public HashMap order(@RequestBody HashMap<String,String> request) {
		    String[] orders = request.get("items").split(",");
			
			// create  a hashmap for finding price of an item using item_id 
			// key is item_id , value is price for that item
			HashMap<Integer, Integer> prices = new HashMap();
			
			// get the menu items
			List<Menu> menu = userService.showMenu();
			
			// iterate over all items in menu and update the hashmap
			for(Menu item:menu) {
				prices.put(item.getId(), item.getPrice());
			}
			
			int total = 0; // intialize the total bill
			
			// iterate over ordered items and calculate total bill
			for(String id : orders) {
				total += prices.get(Integer.parseInt(id)) ;
			}
			
			//create Bill object with current total_bill
			Bill bill = new Bill(total) ;
			
			//save the bill object
			userService.save(bill) ;
			HashMap<String, String> res = new HashMap();
			res.put("bill", "Total bill is Rs." + total);
			return res;
		
	}
	
}

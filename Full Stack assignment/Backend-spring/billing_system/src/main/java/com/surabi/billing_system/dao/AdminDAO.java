package com.surabi.billing_system.dao;

public interface AdminDAO {
	
	public boolean isValid(String theName, String thePassword);

}

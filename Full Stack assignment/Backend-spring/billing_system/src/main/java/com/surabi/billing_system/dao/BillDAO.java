package com.surabi.billing_system.dao;

import com.surabi.billing_system.entities.Bill;

public interface BillDAO {

	public void save(Bill bill);
	public int getMonthBill();
	public int getDayBill();
	
}

package com.surabi.billing_system.dao;

import java.util.List;

import org.hibernate.*;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.surabi.billing_system.entities.Bill;

@Repository
public class BillDAOImpl implements BillDAO {

	private EntityManager entityManager;
	
	@Autowired
	public BillDAOImpl( EntityManager theEntityManager ) {
		entityManager = theEntityManager;
	}
	
	@Override
	public void save(Bill bill) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(bill);		
	}
	
	@Override
	public int getDayBill() {

		Session currentSession1 = entityManager.unwrap(Session.class);
		Query theQuery = currentSession1.createQuery("from Bill where day(datetime) = day(CURRENT_TIMESTAMP) and month(datetime) = month(CURRENT_TIMESTAMP) and year(datetime) = year(CURRENT_TIMESTAMP)");

		List<Bill> ls = theQuery.getResultList();
		int total = 0;
		
		// iterate over query_result and calculate price 
		for(Bill bill: ls) {
			total += bill.getPrice();
		}
		
		// return total bill
		return total;
	}
	
	@Override
	public int getMonthBill() {
		
		Session currentSession1 = entityManager.unwrap(Session.class);
		Query theQuery = currentSession1.createQuery("from Bill where month(datetime) = month(CURRENT_TIMESTAMP) and year(datetime) = year(CURRENT_TIMESTAMP)");
		List<Bill> ls = theQuery.getResultList();
		
		int total = 0;
		
		// iterate over query_result and calculate price 
		for(Bill bill: ls) {
			total += bill.getPrice();
		}
		// return total bill
		return total;
	}
	
}

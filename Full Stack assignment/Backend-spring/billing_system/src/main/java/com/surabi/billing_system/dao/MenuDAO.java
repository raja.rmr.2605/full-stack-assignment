package com.surabi.billing_system.dao;

import java.util.List;

import com.surabi.billing_system.entities.Menu;

public interface MenuDAO {

	public List<Menu> findAll();
	
}

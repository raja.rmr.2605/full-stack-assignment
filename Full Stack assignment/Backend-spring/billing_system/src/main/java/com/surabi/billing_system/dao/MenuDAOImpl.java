package com.surabi.billing_system.dao;

import java.util.List;

import org.hibernate.*;
import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.surabi.billing_system.entities.Menu;

@Repository
public class MenuDAOImpl implements MenuDAO {
	
	private EntityManager entityManager ;
	
	@Autowired
	public MenuDAOImpl(EntityManager theEntityManager ) {
		entityManager = theEntityManager ;
	}
	
	// ---------------- returns all menu items------------
	@Override 
	public List<Menu> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Menu> theQuery = currentSession.createQuery("from Menu");
		List<Menu> menu = theQuery.getResultList();
		return menu;
	}
}

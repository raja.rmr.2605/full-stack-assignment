package com.surabi.billing_system.dao;

import com.surabi.billing_system.entities.User;

public interface UserDAO {
	
	public void save(User theUser);
	public void deleteById(int theId );
	public User findById(int theId);
	public boolean isValid(String theName, String thePassword);
	public void update(User theUser);
}

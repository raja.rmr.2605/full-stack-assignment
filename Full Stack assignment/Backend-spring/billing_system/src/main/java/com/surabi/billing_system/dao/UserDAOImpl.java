package com.surabi.billing_system.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.surabi.billing_system.entities.Admin;
import com.surabi.billing_system.entities.User;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO{

	EntityManager entityManager ;
	
	@Autowired
	public UserDAOImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager ;
	}
	
    
	@Override
    public void save(User theUser) {
    	Session currentSession = entityManager.unwrap(Session.class); 
    	currentSession.save(theUser);
    }
	

	@Override
    public void update(User theUser) {
    	Session currentSession = entityManager.unwrap(Session.class); 
    	currentSession.update(theUser);
    }
	
	@Override
	public void deleteById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class); 
		
		// create a query
		Query theQuery = currentSession.createQuery("delete from User where id = :userid");
		// set parameters to query
		theQuery.setParameter("userid", theId);
		theQuery.executeUpdate();
	}

    //-----returns the user with given id------
	@Override
	public User findById(int theId) {
		Session currentSession = entityManager.unwrap(Session.class); 
		return currentSession.get(User.class, theId);
	}
	
	// -----checks whether given credentials are valid or not--------
	@Override
	public boolean isValid(String theName, String thePassword) {
		
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query theQuery = currentSession.createQuery("from User where name = :name and password = :password");
		theQuery.setParameter("name", theName);
		theQuery.setParameter("password", thePassword);
		
		List<Admin> ls = theQuery.getResultList();
		
		if(ls.size()==0) {
			return false;// if size of result is zero, it means no records found with given credentials
		}
		return true;
	}
}

package com.surabi.billing_system.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.surabi.billing_system.dao.AdminDAO;
import com.surabi.billing_system.dao.BillDAO;

@Service
public class AdminServiceImpl implements AdminService {
	@Autowired
	BillDAO billDao;
	
	@Autowired
	AdminDAO adminDAO;
	
	@Override
	public boolean isValid(String username, String password) {
		return adminDAO.isValid(username, password) ;
	}
	

	@Override
	public int getMonthBill() {
		return billDao.getMonthBill() ;
	}
	
	@Override
	public int getDayBill() {
		return billDao.getDayBill();
	}
}

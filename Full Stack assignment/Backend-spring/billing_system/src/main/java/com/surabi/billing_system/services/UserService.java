package com.surabi.billing_system.services;

import java.util.List;

import com.surabi.billing_system.entities.Bill;
import com.surabi.billing_system.entities.Menu;
import com.surabi.billing_system.entities.User;

public interface UserService {
	
	public boolean isValid(String name, String password);
	public User findById(int id);
	public void save(User user);
	public void update(User user);
	public void deleteById(int id);
	public List<Menu> showMenu();
	public void save(Bill bill);
}

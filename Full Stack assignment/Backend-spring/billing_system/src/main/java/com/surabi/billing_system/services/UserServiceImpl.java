package com.surabi.billing_system.services;

import java.util.List ;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.surabi.billing_system.dao.BillDAO;
import com.surabi.billing_system.dao.MenuDAO;
import com.surabi.billing_system.dao.UserDAO;
import com.surabi.billing_system.entities.Bill;
import com.surabi.billing_system.entities.Menu;
import com.surabi.billing_system.entities.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDAO userDao ;
	
	@Autowired 
	MenuDAO menuDao ;
	
	@Autowired 
	BillDAO billDao ;
	
	@Override
	public boolean isValid(String name, String password) {
		return userDao.isValid(name, password);
	}
	
	@Override
	public User findById(int id) {
		return userDao.findById(id);
	}
	
	@Override
	public void save(User user) {
		userDao.save(user);
	}
	@Override
	public void update(User user) {
		userDao.update(user);
		
	}
	@Override
	public void deleteById(int id) {
		userDao.deleteById(id);
	}
	
	@Override 
	public List<Menu> showMenu(){
		return menuDao.findAll();
	}
	
	@Override
	public void save(Bill bill) {
		billDao.save(bill);
	}
	
	
}

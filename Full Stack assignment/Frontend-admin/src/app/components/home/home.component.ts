import { Component, Input, Output,EventEmitter, OnInit} from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private adminService:AdminService ){} 


  @Input() loggedIn : boolean;
  @Output() loginEmitter : EventEmitter<boolean> = new EventEmitter();
  
  dayBill:string ;
  monthBill:string ;

  username:string;
  password:string;
  id:string;
  user:any;
  updateId:string;
  updatepassword:string;
  updateusername:string;
  deleteid:string;
  deleteUser:any;
  ngOnInit(): void {
    this.adminService.getDayBill().subscribe((res)=>{
      console.log("....")
      console.log(res)
      this.dayBill = res['res']
    })

    this.adminService.getMonthBill().subscribe((res)=>{
      console.log("....")
      console.log(res)
      this.monthBill = res['res']
    })
  }

  logout(){
    this.loggedIn = false ;
    this.loginEmitter.emit(this.loggedIn);
  }

  addUserHandler(){
    console.log(this.username+this.password);
    this.adminService.addUser(this.username, this.password).subscribe((res)=>{
      console.log(res)
    });
  }

  getByIdHandler(){
    this.adminService.getById(this.id).subscribe((res)=>{

        this.user = res;
      
    })
  }

  updatehandler(){
    console.log("update clicked")
    this.adminService.update(this.updateId, this.updateusername, this.updatepassword).subscribe((res)=>console.log(res));
  }

  deletehandler(){
    this.adminService.delete(this.deleteid).subscribe((res)=>{
        console.log(res);
        this.deleteUser=res;
    })
  }

}

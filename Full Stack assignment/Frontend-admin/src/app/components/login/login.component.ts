import { Component, Input, Output,EventEmitter, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  constructor(private adminService:AdminService ) { }
  @Input() loggedIn : boolean;
  @Output() loginEmitter : EventEmitter<boolean> = new EventEmitter();
  loginForm = new FormGroup({
    username:new FormControl('',[Validators.required]),
    password:new FormControl('',[Validators.required,Validators.minLength(6)])
  })

  ngOnInit(): void {
  }


  login(){
    let response:boolean = false;
    console.log(this.loginForm.get('username').value, this.loginForm.get('password').value)
    this.adminService.isValid(this.loginForm.get('username').value, this.loginForm.get('password').value)
    .subscribe((res)=>{
      if(res['loggedin'] ){
        this.loggedIn = true ;
        this.loginEmitter.emit(this.loggedIn);
      }
    })


  }


  getUserName(){
    return this.loginForm.get('username');
  }
  getPassword(){
    return this.loginForm.get('password');
  }
  getUserErrorMsg(){
    if(this.getUserName()?.invalid && (this.getUserName()?.dirty || this.getUserName()?.touched)){
      return "Please enter the valid username"
    }
    return "";
    
  }
  getPasswordErrorMsg(){
    if(this.getPassword()?.invalid &&(this.getPassword()?.dirty || this.getPassword()?.touched)){
      if(this.getPassword()?.hasError('required'))
        return 'The password is Required'
      else if(this.getPassword()?.hasError('minlength'))
        return "The password should have min length 6 chars"
      else 
        return "";
    }
    return "";
  }


}

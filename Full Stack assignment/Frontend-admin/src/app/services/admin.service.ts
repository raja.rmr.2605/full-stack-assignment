import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  constructor(private http:HttpClient) { }

  login:string = "http://localhost:8080/admin/logIn";
  dayBill:string = "http://localhost:8080/admin/dayBill";
  monthBill:string = "http://localhost:8080/admin/monthBill";
  add:string = "http://localhost:8080/admin/users";
  read:string = "http://localhost:8080/admin/users/";

  isValid(username:string, password:string){
    return this.http.post( this.login, {  "name":username, "password":password  }); 
  }
  getDayBill(){
    return this.http.get(this.dayBill);
  }
  getMonthBill(){
    return this.http.get(this.monthBill);
  }

  addUser(username:string, password:string){
    return this.http.post( this.add, {  "name":username, "password":password  }); 
  }
  getById(id:string){
    return this.http.get(this.read + id );
  }
  update(id:string, username:string, password:string){
    return this.http.put(this.add, {
      "id":id,
      "name":username,
      "password":password
    } );
  }

  delete(id:string){
    return this.http.delete(this.read+id);
  }
}

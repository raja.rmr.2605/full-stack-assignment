import './App.css';
import Home from './Home';
import Auth from './Auth';
import { useState } from 'react';

function App() {
  const [ loggedIn, setLoggedIn ] = useState(false);
  
  const flipLogin = () => {
    setLoggedIn(!loggedIn);
  }

  return (
    <div>
    {
      loggedIn ? 
      (<Home  loggedIn={loggedIn} flipLogin={flipLogin} />)
       :
      (<Auth loggedIn={loggedIn} flipLogin={flipLogin} />)
    }
    </div>
  );
}

export default App;

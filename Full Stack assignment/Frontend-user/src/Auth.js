import React from 'react'
import { useState } from 'react'
import './App.css';

export default function Auth({loggedIn, flipLogin}) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const loginHandler = (e)=>{
        e.preventDefault();
        console.log('logging...')
        console.log(username,password);
        fetch('http://localhost:8080/user/logIn', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'name':username,
                'password':password
            }),
        })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            flipLogin();
            })
        .catch((error) => {
            console.error('Error:', error);
        });
        
    }
    const registerHandler = (e)=>{
        e.preventDefault();
        console.log('registering...')
        fetch('http://localhost:8080/user/register', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'name':username,
                'password':password
            }),
        })        
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            })
        .catch((error) => {
            console.error('Error:', error);
        });
    }
    return (
        <div class="container">
            <input class="form-control" type='text' placeholder="username" value={username} onChange={(e)=>setUsername(e.target.value)} />
            <input class="form-control" type='password' placeholder="password" value={password} onChange={(e)=>setPassword(e.target.value)} />
            <button class="btn btn-outline-primary" onClick={loginHandler} >LOGIN</button>
            <button class="btn btn-outline-primary" onClick={registerHandler} >REGISTER</button>
        </div>
    )
}

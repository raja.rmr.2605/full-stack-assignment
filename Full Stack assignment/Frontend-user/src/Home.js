import React from "react";
import { useState, useEffect } from "react";
import "./App.css";
function Home({ loggedIn, flipLogin }) {
  const [menu, setMenu] = useState([]);
  const [bill, setBill] = useState(0);
  const [order, setOrder] = useState("");
  useEffect(() => {
    fetch("http://localhost:8080/user/menu")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setMenu(data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }, []);

  const takeOrder = (e) => {
    e.preventDefault();
    fetch("http://localhost:8080/user/order", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        items: order,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        alert(data.bill);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  return (
    <div>
      <div class="box">
        <button class="btn btn-outline-primary" onClick={flipLogin}>
          LOGOUT
        </button>
      </div>
      <div class="container">
        <h1> MENU </h1>
        {menu.map((item, key) => (
          <h2>
            {item.id}. {item.itemName} => Rs.{item.price}
          </h2>
        ))}
      </div>
      <div class="box">
        <input
          class="form-control"
          type="text"
          placeholder="Enter IDs (coma seperated IDs )"
          value={order}
          onChange={(e) => setOrder(e.target.value)}
        />
        <button class="btn btn-outline-primary" onClick={takeOrder}>
          order
        </button>
      </div>
    </div>
  );
}

export default Home;
